// Gulpfile
var gulp = require('gulp');
var fileinclude = require('gulp-file-include');

gulp.task('copy', function() {
  gulp.src('app/index.html')
  .pipe(gulp.dest('dist'))
});


gulp.task('include', () => {
  gulp.src(['app/index.html'])
    .pipe(fileinclude({
      prefix: '@@',
      basepath: '@file'
    }))
    .pipe(gulp.dest('./dist'))
})



gulp.task('includeall', () => {
  gulp.src(['app/*.html'])
    .pipe(fileinclude({
      prefix: '@@',
      basepath: '@file'
    }))
    .pipe(gulp.dest('./dist'))
})